
import React from 'react';
import { Switch, Route } from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import createHistory from 'history/createHashHistory';
import store from './store';

import Home from './views/home';
import Login from './views/login'
import UserList from './views/userlist';
import Register from './views/register';
import User from './views/user';

const history = createHistory();

if (!window) {
  window = global;
}

const App = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <MuiThemeProvider>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/login' render={(props) => (
              <Login {...props} usedFor='user' />)}
          />
            <Route exact path='/admin' render={(props) => (
                <Login {...props} usedFor='admin' />)}
            />
          <Route exact path='/userList' component={UserList} />
          <Route exact path='/register' component={Register} />
          <Route exact path='/user' component={User} />
        </Switch>
      </MuiThemeProvider>
    </ConnectedRouter>
  </Provider>
);

export default App;
