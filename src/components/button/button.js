import React from 'react';
import cn from 'classnames';
import RaisedButton from 'material-ui/RaisedButton';
import './button.scss';

const style = {
    width: '100%',
    marginTop: '20px',
}

const Button = (props) => (
    <RaisedButton
        label={props.label}
        onClick={props.onClick}
        fullWidth={true}
        style={style}
        backgroundColor="rgb(27, 15,102)"
        labelColor="white"
    />
)

export default Button;
