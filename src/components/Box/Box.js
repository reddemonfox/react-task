import React from 'react';

import './style.scss';

const Box = (props) => (
    <div className="col-xs-12 box_boxContainer">
        <div className="col-xs-7 box_inputContainer">
        <span className="spanVertical">
            {props.children}
        </span>
        </div>
        <div className="col-xs-5 box_sideImage">

        </div>
    </div>
);

export default Box;
