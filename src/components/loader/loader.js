import React, { Component } from 'react';
import Modal from 'react-responsive-modal';
import CircularProgress from 'material-ui/CircularProgress';

const style = {
    modal: {
        background: 'none',
        border:'1px solid red',
    }
}

class FullScreenLoader extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: true,
            title: '',
            url: '',
            showCloseIcon: false,
        };
    }


    render() {
        const modalStyle = {
            width: '100%',
            padding: '0',
            background: 'none',
            boxShadow: 'none',
            textAlign: 'center',
        };
        const { open, showCloseIcon } = this.state;
        return (
            <div>
                <Modal modalStyle={modalStyle} showCloseIcon={showCloseIcon} open={open} little>
                    <CircularProgress color="red" size={40} />
                </Modal>
            </div>
        );
    }
}

export default FullScreenLoader;
