import React from 'react';

import TextField from 'material-ui/TextField';

const styles = {
  floatingLabelStyle: {
    fontSize: '10px',
  },
  errorStyle: {
    textAlign: 'left'
  }
}


const InputField = props => {
  const { onChange, onFocus, type, name, value, label, errorText, errorName } = props;
  return (
    <div>
      <TextField
        floatingLabelText={label}
        floatingLabelStyle={styles.floatingLabelStyle}
        onChange={val => onChange(name, val.target.value, errorName)}
        onFocus={onFocus}
        errorText={errorText}
        errorStyle={styles.errorStyle}
        type={type}
        fullWidth={true}
      /><br />
    </div>
  )
};

export default InputField;

