import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
// import * as reducers from './reducers';
import { home } from '../../views/home/home.redux';
import { login } from '../../views/login/login.redux';
import { userlist } from '../../views/userlist/userlist.redux';
import { register } from '../../views/register/register.redux';
import { userData } from "../../views/user/user.redux";

const rootReducer = combineReducers({
    home,
    login,
    userlist,
    register,
    userData,
 });


export { default as createReducer } from './createReducer';
export default rootReducer;
