
import { Map } from 'immutable'; // use of immutablejs for various js operations to simplify our work
import { has } from 'ramda';
import { createStructuredSelector } from 'reselect';

// import { createReducer } from '../../common/reducers';

import {
    INIT_HOME,
} from './home.actionTypes';

// MAINTAIN INTIALSTATE OF REDUX
export const initialState = Map({
    isLoading: false,
});

// CREATE ALL HANDLERS
const handlers = {
    [INIT_HOME](state) {
        return state.merge({
            isLoading: true,
        });
    },
}



function createReducer(initialState: InitialState, handlers: Handlers) {
    return (state: InitialState = initialState, action: Action = {}) => {
        if (has(action.type, handlers)) {
            return handlers[action.type](state, action);
        }
        return state;
    };
}



export const home = createReducer(initialState, handlers);


// HOME SELECTOR
const getIsLoading = state => state.home.get('isLoading');

export const registerSelector = createStructuredSelector({
    isLoading: getIsLoading,
});
