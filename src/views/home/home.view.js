import React, { Component } from 'react';

class Home extends Component {
    render() {
        return (
            <div className="home_page">
                <h1>Welcome!</h1>
            </div>
        )
    }
}

export default Home;
