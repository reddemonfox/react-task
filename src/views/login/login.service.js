import { fetchJSON } from '../../common/https/serviceHandler';

export default function loginUser(payload, user){
      if(user ==='admin') {
          return fetchJSON('http://poc.luezoid.com/admin/v1/authenticate', 'POST', payload);
      }else {
          return fetchJSON('http://poc.luezoid.com/api/v1/authenticate', 'POST', payload);
      }
}
