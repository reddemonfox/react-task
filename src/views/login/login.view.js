import React, { Component } from 'react';
import { connect } from 'react-redux'; // to connect to redux store from containers redux.js or any other redux store
import validator from '../../common/utils/validator';
import renderIf from 'render-if';
import Error from '../../components/error/Error';
import Loader from '../../components/loader/loader';
import { loginSelector } from './login.redux';
import authenticateUser from './login.actions';
import InputField from '../../components/InputField';
import Button from '../../components/button';
import Box from '../../components/Box';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: 'admin@admin.com',
            password: 'asdf@1234'
        };
    }

    componentWillMount(){
        const userToken = window.localStorage.getItem('userToken');
        const token = window.localStorage.getItem('token');
        if(userToken !== null || token !== null) {
            if (token.length > 0) {
                this.props.history.push('/userList');
            } else if (userToken.length > 0) {
                this.props.history.push('/user');
            }
            debugger;
        }
    }

    componentWillReceiveProps(np){
        const userToken = window.localStorage.getItem('userToken');
        const token = window.localStorage.getItem('token');
        if(np.userData.size > 0 && (userToken !== null || token !== null) ){
            if(this.props.usedFor === 'user') {
                this.props.history.push('/user');
            } else{
                this.props.history.push('/userList');
            }
        }
        debugger;
    }

    handleChange = (name, value, error) => {
        this.setState({ [name]: value, [error]: '' });
    };

    handleLogin = () => {

        const payload = {
            email: this.state.email,
            password: this.state.password,
        };
        const validatedData = validator(payload);
        if (validatedData.emailIsValid && validatedData.passwordIsValid) {
            this.props.authenticateUser(payload, this.props.usedFor);
        } else {
            this.setState(validatedData);
        }
    };

    render() {
        const { email, password, emailErrorMessage, passwordErrorMessage } = this.state;
        const { isLoading,err } = this.props;
        const renderLoader = renderIf(isLoading);
        const renderErrorMessage = renderIf(err !== '');
        return (
            <Box>
                {renderLoader(<Loader />)}
                <div>
                    <h5>SIGN IN</h5>
                    {renderErrorMessage(<Error error={err} />)}
                    <InputField
                        onChange={this.handleChange}
                        name="email"
                        label="ENTER EMAIL"
                        value={email}
                        type="text"
                        errorText={emailErrorMessage}
                        errorName="emailErrorMessage"
                    />
                    <InputField
                        onChange={this.handleChange}
                        name="password"
                        label="PASSWORD"
                        value={password}
                        type="password"
                        errorText={passwordErrorMessage}
                        errorName="passwordErrorMessage"
                    />
                    <Button label="SIGN IN" onClick={() => this.handleLogin()} />
                </div>
            </Box>
        )
    }
}

const mapDispatchToProps = {
    authenticateUser
};

export default connect(loginSelector, mapDispatchToProps)(Login);
