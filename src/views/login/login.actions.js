import { fromJS } from 'immutable'; // use of immutablejs for various js operations to simplify our work
import loginUser from './login.service';

import {
    REQUESTED,
    LOGIN_SUCCESSFUL,
    LOGIN_FAILED,
} from './login.actionTypes';

import {
    USER_AUTHENTICATED
} from "../user/user.actionTypes";

const authenticateUser = (payload, user) => (dispatch) => {
  dispatch({ type: REQUESTED });
  loginUser(payload, user)
      .then((res) => {
          window.localStorage.setItem('token', res.data.token);
          if(user === 'user'){
              window.localStorage.setItem('userToken', res.data.token);
          }
          dispatch({
              type: LOGIN_SUCCESSFUL,
              user: res.data.user,
          });

          dispatch({
              type: USER_AUTHENTICATED,
              user: res.data.user,
          });
      })
      .catch((err) => {
          if (err instanceof Error) {
              dispatch({
                  type: LOGIN_FAILED,
                  data: err.message,
              })
          }
      })
};

export default authenticateUser;
