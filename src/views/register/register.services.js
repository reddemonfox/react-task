import { fetchJSON, uploadFiles } from '../../common/https/serviceHandler';

export const uploadFile = (payload) => {

    return uploadFiles('http://poc.luezoid.com/api/v1/files', 'POST', payload);
};
export const register = (payload) => {
    return fetchJSON('http://poc.luezoid.com/api/v1/register', 'POST', payload);
};

