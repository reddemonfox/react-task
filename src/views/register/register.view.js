import React, { Component } from 'react';
import { connect } from 'react-redux'; // to connect to redux store from containers redux.js or any other redux store
import Loader from '../../components/loader/loader';
import Error from '../../components/error/Error';
import renderIf from 'render-if';
import validator from '../../common/utils/validator';
import { selector } from './register.redux';
import registerUser from './register.actions';
import InputField from '../../components/InputField';
import Button from '../../components/button';
import Box from '../../components/Box';
import Modal from '../../components/modal/Modal';
import './register.style.scss';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
        };
    }

    componentWillMount(){
        const userToken = window.localStorage.getItem('userToken');
        const token = window.localStorage.getItem('token');
        if(userToken !== null || token !== null) {
            if (token.length > 0) {
                this.props.history.push('/userList');
            } else if (userToken.length > 0) {
                this.props.history.push('/user');
            }
        }
    }

    componentWillReceiveProps(np){
        if(np.userAdded)this.setState({ showModal: true })
    }

    handleChange = (name, value, error) => {
        this.setState({ [name]: value, [error]: '' });
    };

    storeFile = (event) => {
        let data = new FormData();
        data.append('file', event.target.files[0]);
        this.setState({ resume: data })
    };

    handleRegister = () => {
        const { firstName, lastName, email, resume, workProfileUrl } = this.state;
        const payload = {
            firstName: firstName,
            lastName: lastName,
            email: email,
            workProfileUrl: workProfileUrl,
            resume: resume
        };
        const validatedData = validator(payload);
        if (validatedData.firstNameIsValid && validatedData.lastNameIsValid
            && validatedData.emailIsValid && validatedData.workProfileUrlIsValid) {
            this.props.registerUser(payload);
        } else {
            this.setState(validatedData);
        }

    };

    render() {
        const { firstName, lastName, email,firstNameErrorMessage, lastNameErrorMessage, showModal,
            emailErrorMessage, fileErrorMessage, workProfileUrl, workProfileUrlErrorMessage } = this.state;
        const { isLoading, err } = this.props;
        const renderModel = renderIf(showModal);
        const renderLoader = renderIf(isLoading);
        const renderErrorMessage = renderIf(err !== '');
        return (
            <Box>
                {renderModel(<Modal history={this.props.history} />)}
                {renderLoader(<Loader/>)}
                <div>
                    <h5>SIGN UP</h5>
                    {renderErrorMessage(<Error error={err} />)}
                    <InputField
                        onChange={this.handleChange}
                        name="firstName"
                        label="FIRST NAME"
                        value={firstName}
                        type="text"
                        errorText={firstNameErrorMessage}
                        errorName="firstNameErrorMessage"
                    />
                    <InputField
                        onChange={this.handleChange}
                        name="lastName"
                        label="LAST NAME"
                        value={lastName}
                        type="text"
                        errorText={lastNameErrorMessage}
                        errorName="lastNameErrorMessage"
                    />
                    <InputField
                        onChange={this.handleChange}
                        name="email"
                        label="EMAIL"
                        value={email}
                        type="email"
                        errorText={emailErrorMessage}
                        errorName="emailErrorMessage"
                    />
                    <InputField
                        onChange={this.handleChange}
                        name="workProfileUrl"
                        label="WORK PROFILE URL"
                        value={workProfileUrl}
                        errorText={workProfileUrlErrorMessage}
                        errorName="workProfileUrlErrorMessage"
                    />
                    <div className="file_input">
                    <input
                        onChange={this.storeFile}
                        type="file"
                        accept="application/pdf"
                    />
                    </div>
                    <Button label="REGISTER" onClick={this.handleRegister} isLoading={isLoading} />
                </div>
            </Box>
        )
    }
}

const mapDispatchToProps = {
    registerUser
};

export default connect(selector, mapDispatchToProps)(Register);
