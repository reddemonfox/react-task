import { fromJS } from 'immutable'; // use of immutablejs for various js operations to simplify our work
import { uploadFile, register } from './register.services';

import {
    REQUESTED,
    SUCCESSFULL,
    FAILED,
} from './register.actionTypes';

const registerUser = (payload) => (dispatch) => {
  dispatch({ type: REQUESTED });

  uploadFile(payload.resume)
      .then((res) => {
          console.log(res);
          Object.assign(payload, {resumeId: res.object.id});
          register(payload)
              .then( (res) => {
                  if(res.object !== null) {
                      dispatch({
                          type: SUCCESSFULL,
                          data: res,
                      })
                  }else{
                      dispatch({
                          type: FAILED,
                          data: res.message,
                      })
                  }
              }).catch( (err) => {
              if (err instanceof Error) {
                  dispatch({
                      type: FAILED,
                      data: err.message,
                  })
              }
          });
      })
      .catch((err) => {
          if (err instanceof Error) {
              dispatch({
                  type: FAILED,
                  data: err.message,
              })
          }
      })
};

export default registerUser;
