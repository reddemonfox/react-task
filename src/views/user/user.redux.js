
import { Map } from 'immutable'; // use of immutablejs for various js operations to simplify our work
import { createStructuredSelector } from 'reselect/src';
import { createReducer } from '../../common/reducers';

import {
    USER_AUTHENTICATED,
} from './user.actionTypes';


// MAINTAIN INTIALSTATE OF REDUX
export const initialState = Map({
    userData: Map(),
});


// CREATE ALL HANDLERS
const handlers = {
    [USER_AUTHENTICATED](state, action) {
        return state.merge({
            userData: action.user
        });
    },
};


// BIND ALL HANDLERS AND INITIAL STATE
export const userData = createReducer(initialState, handlers);


// HOME SELECTOR
const getUserData = state => state.userData.get('userData');


export const selector = createStructuredSelector({
    userData: getUserData,
});
