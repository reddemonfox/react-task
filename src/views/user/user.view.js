import React, { Component } from 'react';
import { connect } from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import { selector } from './user.redux';
class User extends Component {
    constructor(props){
        super(props);
        this.state = {};
    }
    componentWillMount(){
        const userToken = window.localStorage.getItem('userToken');
        if(userToken === null) {
                this.props.history.push('/');
        }
    }

    componentWillReceiveProps(np){
        const userToken = window.localStorage.getItem('userToken');
        if(!np.userData.size > 0 && userToken !== null){
            this.props.history.push('/');
        }
    }
    logout = () => {
        debugger;
        window.localStorage.clear();
        this.props.history.push('/login');
    };

    render(){
        return(
            <div className="col-xs-12">
                <h4>Valid User</h4>
                <RaisedButton label="Logout" onClick={() => this.logout()} />
            </div>
        )
    }
}

export default connect(selector, {})(User);
